<?php

namespace kfit\api\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\base\NotSupportedException;
use yii\db\Schema;

/**
 * Modelo base
 *
 * @package kfit
 * @subpackage api
 * @category Components
 *
 * @property-read string STATUS_ACTIVE Estado activo según la columna.
 * @property-read string STATUS_INACTIVE Estado inactivo según la columna.
 * @property-read string STATUS_COLUMN Nombre de columna de estado.
 * @property-read string CREATED_BY_COLUMN Nombre de columna de Creado por.
 * @property-read string CREATED_AT_COLUMN Nombre de columna de Fecha creación.
 * @property-read string UPDATED_BY_COLUMN Nombre de columna de Modificado por.
 * @property-read string UPDATED_AT_COLUMN Nombre de columna de Fecha modificación.
 *
 * @property \yii\web\User $creadoPor
 * @property \yii\web\User $modificadoPor
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2021 KulturFit S.A.S.
 *
 */
class Model extends ActiveRecord
{
    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    const STATUS_COLUMN = 'active';
    const CREATED_BY_COLUMN = 'created_by';
    const CREATED_AT_COLUMN = 'created_at';
    const UPDATED_BY_COLUMN = 'updated_by';
    const UPDATED_AT_COLUMN = 'updated_at';
    const DEFAULT_USER_ID = 1;

    public $classNames = [];

    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * @var int a counter used to generate [[id]] for forms.
     * @internal
     */
    public static $counter = 0;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $_dbFields;


    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->{$this::STATUS_COLUMN} = static::STATUS_ACTIVE;
    }

    /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => static::CREATED_BY_COLUMN,
                'updatedByAttribute' => static::UPDATED_BY_COLUMN,
                'defaultValue' => (isset(Yii::$app->params['defaultUserId'])) ? Yii::$app->params['defaultUserId'] : static::DEFAULT_USER_ID,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => [static::CREATED_AT_COLUMN, static::UPDATED_AT_COLUMN],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [static::UPDATED_AT_COLUMN],
                    // 'createdAtAttribute' => static::CREATED_AT_COLUMN,
                    // 'updatedAtAttribute' => static::UPDATED_AT_COLUMN,
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * Método encargado de entregar el nombre de la columna para relaciones
     *
     * @return string
     */
    public static function getNameFromRelations()
    {
        foreach (static::getTableSchema()->columns as $column) {
            if (!$column->isPrimaryKey) {
                return $column->name;
            }
        }
    }

    /**
     * Sobreescribimos para quitar campos de auditoria
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields[static::STATUS_COLUMN], $fields[static::CREATED_BY_COLUMN],
        $fields[static::CREATED_AT_COLUMN],
        $fields[static::UPDATED_BY_COLUMN],
        $fields[static::UPDATED_AT_COLUMN]);

        return $fields;
    }

    /**
     * Define la columna que se mostrará en los formularios para identificar el recurso
     * 
     * @return string
     */
    public static function columnInCrud()
    {
        return static::primaryKey()[0];
    }

    /**
     * Definición de atributos tipo archivo
     * 
     * ```
     * [
     *     'profile_photo' => [
     *         'type' => UploadFilesBehavior::TYPE_FILE,
     *         'modelClass' => Members::class,
     *         'value' => $this->getMember(),
     *         'attribute' => 'profile_photo',
     *         'isMultiple' => false,
     *         'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
     *         'relatedKey' => [
     *             'user_id' => 'user_id',
     *         ],
     *         'relatedAttributes' => [
     *             'user_id' => 'user_id',
     *             'name' => 'name',
     *             'mobile_number' => 'mobile_number',
     *             'document_number' => 'document_number',
     *             'document_type_id' => 'document_type_id',
     *             'address' => 'address',
     *             'city_id'  => 'city_id',
     *             'associated' => function($modelOwner){
     *                  return $modelOwner->associated;
     *              },
     *         ]
     *     ]
     * ];
     * ```
     * 
     * @return array
     */
    public function fileAttributes()
    {
        return [];
    }

    /**
     * @return string[] all db schema names or an array with a single empty string
     * @throws NotSupportedException
     * @since 2.0.5
     */
    protected function getSchemaNames()
    {
        $db     = $this->getDb();
        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }

    /**
     * Checks if the given table is a junction table, that is it has at least one pair of unique foreign keys.
     * @param \yii\db\TableSchema the table being checked
     * @return array|boolean all unique foreign key pairs if the table is a junction table,
     * or false if the table is not a junction table.
     */
    protected function checkJunctionTable($table)
    {
        if (count($table->foreignKeys) < 2) {
            return false;
        }
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDb()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        $result      = [];
        // find all foreign key pairs that have all columns in an unique constraint
        $foreignKeys = array_values($table->foreignKeys);
        for ($i = 0; $i < count($foreignKeys); $i++) {
            $firstColumns = $foreignKeys[$i];
            unset($firstColumns[0]);

            for ($j = $i + 1; $j < count($foreignKeys); $j++) {
                $secondColumns = $foreignKeys[$j];
                unset($secondColumns[0]);

                $fks = array_merge(
                    array_keys($firstColumns),
                    array_keys($secondColumns)
                );
                foreach ($uniqueKeys as $uniqueKey) {
                    if (count(array_diff(
                        array_merge($uniqueKey, $fks),
                        array_intersect(
                            $uniqueKey,
                            $fks
                        )
                    )) === 0) {
                        // save the foreign key pair
                        $result[] = [$foreignKeys[$i], $foreignKeys[$j]];
                        break;
                    }
                }
            }
        }
        return empty($result) ? false : $result;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     * @param array $relations the relations being generated currently.
     * @param \yii\db\TableSchema $table the table schema
     * @param string $key a base name that the relation name may be generated from
     * @param boolean $multiple whether this is a has-many relation
     * @return string the relation name
     */
    protected function generateRelationName($relations, $table, $key, $multiple)
    {
        if (!empty($key) && substr_compare($key, 'id', -2, 2, true) === 0 && strcasecmp(
            $key,
            'id'
        )) {
            $key = rtrim(substr($key, 0, -2), '_');
        }
        if ($multiple) {
            $key = Inflector::pluralize($key);
        }
        $name    = $rawName = Inflector::id2camel($key, '_');
        $i       = 0;
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$table->fullName][$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Método encargado de entregar el comentario de una tabla de la base de datos
     *
     * @param string $tableName Nombre de la tabla (con schema si es necesario)
     * @return string
     */
    public function getComment($tableName)
    {
        $db      = $this->getDb();
        $comment = '';

        if ($db->driverName == 'pgsql') {
            $command = $db->createCommand("SELECT obj_description('{$tableName}'::regclass, 'pg_class') AS comment");
            $comment = $command->queryOne()['comment'];
        } elseif ($this->getDb()->driverName == 'mysql') {
            $query = new Query;
            $query->select('table_comment as comment');
            $query->from('INFORMATION_SCHEMA.TABLES');

            if (strpos($tableName, '.') !== false) {
                $tableNaname = explode('.', $tableName);
                $schema      = $tableNaname[0];
                $table       = $tableNaname[1];
                $query->andWhere(['table_schema' => $schema]);
                $query->andWhere(['table_name' => $table]);
            } else {
                $query->andWhere(['table_name' => $tableName]);
            }
            $comment = $query->one()['comment'];
        }


        return $comment;
    }

    /**
     * Generates the link parameter to be used in generating the relation declaration.
     * @param array $refs reference constraint
     * @return string the generated link parameter.
     */
    protected function generateRelationLink($refs)
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = "'$a' => '$b'";
        }

        return '[' . implode(', ', $pairs) . ']';
    }

    /**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @param boolean $useSchemaName should schema name be included in the class name, if present
     * @return string the generated class name
     */
    protected function generateClassName($tableName, $useSchemaName = null)
    {

        if (isset($this->classNames[$tableName])) {
            return $this->classNames[$tableName];
        }

        $schemaName    = '';
        $fullTableName = $tableName;
        if (($pos           = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

        $db         = $this->getDb();
        $patterns   = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return $this->classNames[$fullTableName] = Inflector::id2camel(
            $schemaName . $className,
            '_'
        );
    }

    /**
     * Determines if relation is of has many type
     *
     * @param TableSchema $table
     * @param array $fks
     * @return boolean
     * @since 2.0.5
     */
    protected function isHasManyRelation($table, $fks)
    {
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDb()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        foreach ($uniqueKeys as $uniqueKey) {
            if (count(array_diff(
                array_merge($uniqueKey, $fks),
                array_intersect($uniqueKey, $fks)
            )) === 0) {
                return false;
            }
        }
        return true;
    }
}
