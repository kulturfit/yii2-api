<?php

namespace kfit\api\actions;

use Yii;

/**
 * UpdateAction base para los crud del sistema
 *
 * @package kfit\api\actions\UpdateAction
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class UpdateAction extends SaveAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'update';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isNewRecord = false;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'It was updated successfully.';
}
