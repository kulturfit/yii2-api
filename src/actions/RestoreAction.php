<?php

namespace kfit\api\actions;

use Yii;
use kfit\api\actions\BaseAction;

/**
 * RestoreAction base para los crud del sistema
 *
 * @package kfit\api\actions\RestoreAction
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class RestoreAction extends UpdateAttributeAction
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'It was successfully restore.';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnError = 'An error occurred while trying to restore.';

    /**
     * Permite cambiar el valor de un atributo de un modelo en el sistema
     *
     * @return void
     */
    public function run($id)
    {
        $modelClass = $this->modelClass;
        if (is_array($this->modelClass)) {
            $primaryClassFound = false;
            foreach ($this->modelClass as $keyModel => $modelConfig) {
                if (isset($modelConfig['isPrimary']) && $modelConfig['isPrimary']) {
                    $modelClass = $modelConfig['class'];
                    $primaryClassFound = true;
                    break;
                }
            }
            if (!$primaryClassFound) {
                throw new InvalidConfigException(Yii::t('app', 'Primary model not found'));
            }
        }
        if (empty($this->value)) {
            $this->value = $modelClass::STATUS_ACTIVE;
        }
        if (empty($this->attribute)) {
            $this->attribute = $modelClass::STATUS_COLUMN;
        }
        return parent::run($id);
    }
}
