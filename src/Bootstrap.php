<?php

namespace kfit\api;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use kfit\api\base\Bootstrap as BootstrapBase;
use kfit\api\validators\CompareValidator as KFitCompareValidator;
use kfit\api\validators\UniqueValidator as KFitUniqueValidator;
use yii\validators\CompareValidator;
use yii\validators\UniqueValidator;

/**
 * Clase cargadora de características para la aplicación.
 *
 * @package kfit
 * @subpackage yii2-base
 * @category Bootstrap
 * 
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 0.0.0
 */
class Bootstrap extends BootstrapBase
{
    public $moduleId = 'api-core';
    public $pieces = [
        'modules' => [
            'api-core' => \kfit\api\Module::class
        ],
        'components' => [
            'strings' => \kfit\api\helpers\StringsHelper::class,
            'arrayHelper' => \kfit\api\helpers\ArrayHelper::class
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        parent::bootstrap($app);

        Yii::$container->set(CompareValidator::class, KFitCompareValidator::class);
        Yii::$container->set(UniqueValidator::class, KFitUniqueValidator::class);

        if ($app instanceof \yii\console\Application) {
            $app->getModule($this->moduleId)->controllerNamespace =
                'kfit\api\commands';
        }

    }
}
