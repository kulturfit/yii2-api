<?php

namespace kfit\api\rest;

use Yii;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use kfit\api\filters\auth\HttpJwtAuth;

/**
 * Controller Implementa las Acciones REST disponibles para los controladores del módulo Api.
 *
 * @package kfit
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class Controller extends \yii\rest\Controller
{

    /**
     * Retorna la lista de behaviors que el controlador implementa en el rest api
     *
     * @return array
     */
    public function behaviors()
    {
        $this->module->cors();

        $behaviors                                                     = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class'       => CompositeAuth::className(),
            'authMethods' => [
                'jwt' => [
                    'class' => HttpJwtAuth::className(),
                    'publicKey' => Yii::$app->params['publicKey'],
                    'privateKey' => Yii::$app->params['privateKey']
                ]
            ],
        ];
        return $behaviors;
    }

}
