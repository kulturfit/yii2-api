<?php

namespace kfit\api\rest\actions;

use Yii;
use yii\web\ServerErrorHttpException;
use yii\rest\Action;

/**
 * DeleteAction Acción eliminar disponible en el API REST.
 *
 * @package kfit
 * @subpackage rest/actions
 * @category Action
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class DeleteAction extends Action
{

    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        if (!empty($id))
        {
            $model = $this->findModel($id);

            if ($this->checkAccess)
            {
                call_user_func($this->checkAccess, $this->id, $model);
            }

            $model->{$model::STATUS_COLUMN} = $model::STATUS_INACTIVE;

            if ($model->update(true, [$model::STATUS_COLUMN]) === false)
            {
                throw new ServerErrorHttpException(Yii::t('app',
                                                          'Failed to delete the object for unknown reason.'));
            }

            return [
                'name'    => Yii::t('app', 'Successful operation'),
                'message' => Yii::t('app',
                                    'The record has been removed successfully.'),
                'code'    => 0,
                'status'  => 200,
                'type'    => '',
            ];
        }
        else
        {
            throw new \InvalidArgumentException(Yii::t('app', 'Invalid item ID'));
        }
    }

}
