<?php

namespace kfit\api\rest\actions;

use yii\web\ServerErrorHttpException;
use yii\rest\Action;

/**
 * RestoreAction Acción restaurar disponible en el API REST.
 *
 * @package kfit
 * @subpackage rest/actions
 * @category Action
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class RestoreAction extends Action
{

    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {

        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $model->{$model::STATUS_COLUMN} = $model::STATUS_ACTIVE;

        if ($model->update() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        return $model;
    }
}
