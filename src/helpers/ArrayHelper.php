<?php

namespace kfit\api\helpers;

use yii\helpers\ArrayHelper as ArrayHelperBase;

/**
 * 
 * @package kfit
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@timakers.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ArrayHelper extends ArrayHelperBase
{
    /**
     * Permite unir varios arreglos de datos en uno solo
     *
     * @param array ...$arrays
     * @return void
     */
    public static function mergeMultiple(...$arrays)
    {
        $value = [];
        foreach ($arrays as $array) {
            $value = parent::merge($value, $array);
        }
        return $value;
    }
}
